
public class Ladung {
	private String bezeichnung;
	private int anzahl;
	
	/**
	 * Konstruktor der Klasse Ladung, welcher g�ltige Standard-Werte vergibt (bezeichnung = "Standard Ladung", anzahl = -1). 
	 */
	public Ladung() {
		bezeichnung = "Standard Ladung";
		anzahl = -1;
	}
	
	/**
	 * Erstellt eine Ladung mit dem gew�nschten Namen und der Menge.
	 * 
	 * @param bezeichnung Der Name der Ladung
	 * @param anzahl Die Anzahl der Ladung
	 */
	public Ladung(String bezeichnung, int anzahl) {
		this.bezeichnung = bezeichnung;
		this.anzahl = anzahl;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	
}
