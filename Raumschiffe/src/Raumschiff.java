import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Madeleine Dinse
 *
 */

public class Raumschiff {
	private String name;
	private int energieversorgung;
	private int schutzschild;
	private int huelle;
	private int lebenserhaltungssysteme;
	private int anzahlAndroiden;
	private int anzahlPhotonentorpedo;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsListe = new ArrayList<Ladung>();

	/**
	 * Dies ist ein leerer Standard- Konstruktor, der g�ltige Standard-Werte zuwei�t, welcher aufgerufen wird wenn keine Parameter in die Klammer geschrieben werden.
	 * (name = "Raumschiff 1", energieversorgung = 100, schutzschild = 100, h�lle = 100, lebenserhaltungssysteme = 100, anzahlAndroiden = 5, anzahlPhotonentorpedos = 5)
	 */
	public Raumschiff() {
		name = "Raumschiff 1";
		energieversorgung = 100;
		schutzschild = 100;
		huelle = 100;
		lebenserhaltungssysteme = 100;
		anzahlAndroiden = 5;
		anzahlPhotonentorpedo = 5;		
	}

	/**
	 * Ein vollparametisierter Konstruktor der mithilfe verschiedener Parameter ein Objekt der Klasse Raumschiff erstellt.
	 * 
	 * @param name Der Name des raumschiffes
	 * @param energieversorgung Die Angabe des Zustandes der Energieversorgung in Prozent
	 * @param schutzschild Die Angabe des Zustandes des Schutzschildes in Prozent
	 * @param huelle Die Angabe des Zustandes der H�lle in Prozent
	 * @param lebenserhaltungssysteme Die Angabe des Zustandes der Lebenserhaltungssysteme in Prozent
	 * @param anzahlAndroiden Die Anzahl der Reperaturandroiden an Bord
	 * @param anzahlPhotonentorpedo Die Anzahl der geladenen Photonentorpedos an Bord (Im Lager k�nnen auch noch welche liegen)
	 */
	public Raumschiff(String name, int energieversorgung, int schutzschild, int huelle, int lebenserhaltungssysteme, int anzahlAndroiden, 
			int anzahlPhotonentorpedo) {
		this.name = name;
		this.energieversorgung = energieversorgung;
		this.schutzschild = schutzschild;
		this.huelle = huelle;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.anzahlAndroiden = anzahlAndroiden;
		this.anzahlPhotonentorpedo = anzahlPhotonentorpedo;		
	}
	
	/**
	 * Diese Methode f�gt der Ladungsliste (ArrayList) ein Objekt der Klasse {@link Ladung} hinzu.
	 * 
	 * @param neueLadung Ein Objekt der Klasse {@link Ladung}.
	 */
	public void addLadung(Ladung neueLadung) {	
		ladungsListe.add(neueLadung);
	}
	
	/**
	 * Diese Methode gibt alle Variablen des Raumschiffs auf der Konsole aus.
	 * 
	 */
	public void zustandAusgeben() {
		System.out.println("Die Werte von " + this.name + "  sind: \r\nEnergieversorgung: " + this.energieversorgung +
			"%  Schutzschild: " + this.schutzschild + "% H�lle: " + this.huelle + "% Lebenserhaltungssysteme: " +
				this.lebenserhaltungssysteme + "%." + "\r\nDie Anzahl der Androiden betr�gt " + this.anzahlAndroiden +
				". Au�erdem gibt es " + this.anzahlPhotonentorpedo + " Photonentorpedos.");
	}
	
	/**
	 * Mit dieser Methode wird durch das Lagerverzeichnis iteriert und dann f�r jede Ladung die Bezeichnung und die Anzahl mithilfe der Getter ({@link Ladung#getBezeichnung()} 
	 * und  {@link Ladung#getAnzahl()}) auf der Konsole ausgegeben.
	 */
	public void ladungsAusgabe() {
		System.out.print("Im Lager befinden sich momentan: ");
		for(Ladung i : ladungsListe) {
			System.out.println(i.getBezeichnung() + " x" + i.getAnzahl()); 
		}
	}
	
	/**
	 * Hier wird die Anzahl der Photonentorpedos des schie�enden Raumschiffes �berpr�ft und wenn diese gr��er 0 ist, dann wird die Methode {@link #treffer(Raumschiff)} 
	 * mit dem feindlichen Raumschiff aufgerufen.  Ebenfalls wird dann die Anzahl der Photonentorpedos um eins verringert. 
	 * Es wird bei beiden F�llen (Anzahl gr��er oder kleiner als 0) die Methode {@link #nachrichtAnAlle(String)} mit dem gew�nschten String aufgerufen.
	 * 
	 * @param r Ein Objekt der Klasse {@link Raumschiff}, welches getroffen werden soll.
	 */
	public void photonentorpedosAbschie�en(Raumschiff r) {
		if(this.anzahlPhotonentorpedo <= 0) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			this.anzahlPhotonentorpedo--;
			treffer(r);
		}
	}
	
	/**
	 * Hier wird die Ernegieversorgung des schie�enden Raumschiffes �berpr�ft und wenn diese gr��er 50 ist, dann wird die Methode {@link #treffer(Raumschiff)} 
	 * mit dem feindlichen Raumschiff aufgerufen. Ebenfalls wird dann die Energieversorgung um 50 verringert. 
	 * Es wird bei beiden F�llen (Energieversorgung gr��er oder kleine als 50) die Methode {@link #nachrichtAnAlle(String)} mit dem gew�nschten String aufgerufen.
	 * 
	 * @param r Ein Objekt der Klasse {@link Raumschiff}, welches getroffen werden soll.
	 */
	public void phaserkanonenAbschie�en(Raumschiff r) {
		if(energieversorgung <= 50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			nachrichtAnAlle("Phaserkanone abgeschossen");
			this.energieversorgung -= 50;
			treffer(r);
		}
	}

	/**
	 * Es wird der Name des getroffenen Raumschiffes ausgegeben. Ebenfalls wird das Schutzschild um 50 verringert. 
	 * Wenn das Schutzschild des Raumschiffes kleiner gleich 0 ist, dann wird die Energieversorgung, sowie die H�lle um 50 verringert. 
	 * Wenn nach diesem Fall dann auch die H�lle kleiner gelich 0 ist, dann wird die Variable Lebenserhaltungssysteme auf 0 gesetzt und {@link #nachrichtAnAlle(String)} wird aufgerufen.
	 * 
	 * @param r Ein Objekt der Klasse {@link Raumschiff}, welches das Ziel des Schusses ist.
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.name + " wurde getroffen!");
		r.setSchutzschild(r.getSchutzschild() - 50);
		if (r.getSchutzschild() <= 0) { //Fortgeschritten und Experte
			r.setEnergieversorgung(r.getEnergieversorgung() - 50);
			r.setHuelle(r.getHuelle() - 50);
			if(r.getHuelle() <= 0) {
				r.setLebenserhaltungssysteme(0);
				nachrichtAnAlle("Lebenserhaltungssysteme vom Raumschiff " + r.getName() + "wurden v�llig zerst�rt.");
			}
		}
	}
	

	/**
	 * 
	 * Hier wird ein String einer ArrayList(broadcastKommunikator) hinzugef�gt
	 * <p>
	 * Wenn man m�chte kann man die Ausgabe auf der Konsole auskommentieren und dann wird die Nachricht ebenfalls auf der Konsole ausgegeben.
	 * @param nachricht Die Nachricht die dem broadcastKommunikator(ArrayList) hinzugef�gt werden soll
	 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
//		System.out.println(nachricht); Anf�nger
	}
	
	/**
	 * 
	 * @return Die ArayList broadcastKommunikator
	 */
	public ArrayList<String> rueckgabeLogbuch() {
		return broadcastKommunikator;
	}
	
	/**
	 * Diese Methode iteriert durch die ALdungsliste und z�hlt die Anzahl aller Photonentorpedos zusammen. 
	 * Bei einer gesamten Anzahl von 0 wird {@link #nachrichtAnAlle(String)} aufgerufen. <p>
	 * Ist dies nicht der Fall wird die Differenz zwischen der gesamten Anzahl und der gew�nschten anzahl berechnet. 
	 * Wenn die Differenz kleiner 0 ist, wird die Anzahl der geladenen Torpedos auf die im Lager vorhandenen gesetzt. 
	 * Die eingesetzte Anzahl wird auf der Konsole ausgegeben und die Funktion {@link #ladungBearbeiten(String, int)} wird aufgerufen. <p>
	 * Ist die Differenz gr��er als 0, dann wird die Anzahl der geladenen Torpedos auf die gew�nscht Anzahl gesetzt.
	 * Hier wird dann ebenfalls die nachgeladenene Anzahl auf der Konsole ausgegeben und die Funktione {@link #ladungBearbeiten(String, int)}.
	 * 
	 * @param anzahl Die gew�nschte Anzahl der nachzuladenen Torpedos
	 */
	public void photonentorpedosLaden(int anzahl) {
		int anzahlLager = 0;
		for(Ladung l : ladungsListe) {
			if (l.getBezeichnung().equals("Photonentorpedos")) {
				anzahlLager += l.getAnzahl();
			}
		}
		
		if (anzahlLager == 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
		
		int differenz = anzahlLager - anzahl;
		
		if(differenz < 0) {
			setAnzahlPhotonentorpedo(anzahlLager);
			System.out.println( anzahlLager + " Photonentorpedo(s) eingesetzt");
			ladungBearbeiten("Photonentorpedos", anzahlLager);
		}
		else {
			setAnzahlPhotonentorpedo(anzahl);
			System.out.println( anzahl + " Photonentorpedo(s) eingesetzt");
			ladungBearbeiten("Photonentorpedos", anzahl);
		}					
	}
	
	/**
	 * 
	 * Hier werden alle Elemente dessen Anzahl 0 ist gel�scht.
	 */
	public void ladungsverzeichnisAufraeumen() {
		ArrayList<Ladung> newLadungsverzeichnis = new ArrayList<>();
        for (Ladung l : ladungsListe) {
            if (l.getAnzahl() != 0) {
                newLadungsverzeichnis.add(l);
            }
        }
        ladungsListe = newLadungsverzeichnis;
    }
	
	/**
	 * Mithilfe einer random Zahl wird berechnet um wieviel die verschiedenen Strukturen repariert werden. 
	 * Die Variablen ( Schutzschilde, Energieversorgung, Schiffsh�lle) werden dann um den berechneten Wert erh�ht.
	 * 
	 * @param schutzschilde boolean ob dies repariert werden soll
	 * @param energieversorgung boolean ob dies repariert werden soll
	 * @param schiffshuelle boolean ob dies repariert werden soll
	 * @param anzahlAndroiden boolean ob dies repariert werden soll
	 */
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlAndroiden) {
		Random r = new Random();
		int zufallszahl = r.nextInt(99) + 1;
		int androidenEingesetzt = 0;
		int schiffstrukturenTrue = 0;
		
		if(schutzschilde == true) schiffstrukturenTrue++;
		if(energieversorgung == true) schiffstrukturenTrue++;
		if(schiffshuelle == true) schiffstrukturenTrue++;
		
		if (anzahlAndroiden > this.anzahlAndroiden) {
			androidenEingesetzt = this.anzahlAndroiden;
		}
		else {
			androidenEingesetzt = anzahlAndroiden;
		}
		
		int berechnung = (zufallszahl*androidenEingesetzt)/schiffstrukturenTrue;
		
		if(schutzschilde == true) setSchutzschild(getSchutzschild() + berechnung);
		if(energieversorgung == true) setEnergieversorgung(getEnergieversorgung() + berechnung);
		if(schiffshuelle == true) setHuelle(getHuelle() + berechnung);
	}
	
	/**
	 * Diese Methode setzt f�r jeden nachgeladenen Photonentorpedo aus dem Lager die Anzahl im Lager auf 0. 
	 * <p>
	 * Pr�ft nicht, ob genug Ladung vorhanden ist. (K�nnte Fehler verursachen)
	 * Die Anzahl muss vorher �berpr�ft werden.
	 * @param s Der Name der Ladung dessen Anzahl auf 0 gesetzt werden soll.
	 * @param anzahl Die Anzahl der nachgeladenen Torpedos und somit zu l�schenden Torpedos.
	 */
	private void ladungBearbeiten(String s, int anzahl) {
		for(Ladung l : ladungsListe) {
			if (l.getBezeichnung().equals(s)) {
				int diff =l.getAnzahl() - anzahl;
				if(diff < 0) {
					l.setAnzahl(0);
					anzahl = diff * -1;
				}
				else {
					l.setAnzahl(diff);
					break;
				}
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getEnergieversorgung() {
		return energieversorgung;
	}

	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	public int getSchutzschild() {
		return schutzschild;
	}

	public void setSchutzschild(int schutzschild) {
		this.schutzschild = schutzschild;
	}

	public int getHuelle() {
		return huelle;
	}

	public void setHuelle(int huelle) {
		this.huelle = huelle;
	}

	public int getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}

	public void setLebenserhaltungssysteme(int lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}

	public int getAnzahlAndroiden() {
		return anzahlAndroiden;
	}

	public void setAnzahlAndroiden(int anzahlAndroiden) {
		this.anzahlAndroiden = anzahlAndroiden;
	}

	public int getAnzahlPhotonentorpedo() {
		return anzahlPhotonentorpedo;
	}

	public void setAnzahlPhotonentorpedo(int anzahlPhotonentorpedo) {
		this.anzahlPhotonentorpedo = anzahlPhotonentorpedo;
	}

	public static ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}			
}

