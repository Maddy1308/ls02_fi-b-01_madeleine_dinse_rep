
public class Haupt {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 2, 1);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 50, 100, 5, 0);
		
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedos", 3));
		
		klingonen.photonentorpedosAbschießen(romulaner);
		romulaner.phaserkanonenAbschießen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch!");
		klingonen.zustandAusgeben();
		klingonen.ladungsAusgabe();
		
		vulkanier.reperaturDurchfuehren(true, true, true, 5);
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		
		klingonen.photonentorpedosAbschießen(romulaner);
		klingonen.photonentorpedosAbschießen(romulaner);
		
		klingonen.zustandAusgeben();
		klingonen.ladungsAusgabe();
		
		romulaner.zustandAusgeben();
		romulaner.ladungsAusgabe();
		
		vulkanier.zustandAusgeben();
		vulkanier.ladungsAusgabe();		
	}

}
